extends Control

func _ready():
	PlayerResources.resources_dict = {
	"oxygen": 100,
	"ship_health": 100,
	"cargo" : 500,
	"money": 10000,
	"fines": 0
	}

func _on_Start_pressed():
	Transition.fade_to_scene("res://Ship.tscn")

func _on_Quit_pressed():
	get_tree().quit()
