extends StaticBody

func _process(delta):
	if $"Timer".is_stopped():
		if $"../Player".transform.origin.y < transform.origin.y or $"../Player".vertical > 0:
			collision_mask = 0
		else:
			collision_mask = 1
	if Input.is_action_pressed("ui_down") and $"../Player".is_on_floor():
		collision_mask = 0
		$"Timer".start()

func _on_Timer_timeout():
	collision_mask = 1
