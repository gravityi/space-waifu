extends Control

func _ready():
	$"Breach/AnimationPlayer".play("Blink")
	$"Oxygen/AnimationPlayer".play("Blink")
	$"Status/AnimationPlayer".play("Blink")
	$"Asteroid/AnimationPlayer".play("Blink")

func _process(delta):
	if $"../../".current_breaches:
		$"Breach".show()
	else:
		$"Breach".hide()
	
	if PlayerResources.resources_dict["ship_health"] < 25:
		$"Status".show()
	else:
		$"Status".hide()
	
	if PlayerResources.resources_dict["oxygen"] < 25:
		$"Oxygen".show()
	else:
		$"Oxygen".hide()
	
	if PlayerResources.asteroid_incoming:
		$"Asteroid".show()
	else:
		$"Asteroid".hide()
	
	if PlayerResources.toll_gate:
		$"Toll".show()
	else:
		$"Toll".hide()
	
	$"Time".text = "Time to Destination: " + str($"../../".progress)
	$"CargoBar".value = PlayerResources.resources_dict["cargo"]
	$"HullBar".value = PlayerResources.resources_dict["ship_health"]
	$"OxygenBar".value = PlayerResources.resources_dict["oxygen"]
	$"FinesLabel".text = str(PlayerResources.resources_dict["fines"]) + " / " + str(PlayerResources.resource_limits["fines"])
