extends Area2D

func _on_EngineeringRoom_body_entered(body):
	if body.is_in_group("player"):
		$"ActionTimer".start()

func _on_EngineeringRoom_body_exited(body):
	if body.is_in_group("player"):
		$"ActionTimer".stop()

func _on_ActionTimer_timeout():
	PlayerResources.resources_dict["ship_health"] = clamp(PlayerResources.resources_dict["ship_health"] + 1, 0, PlayerResources.resource_limits["ship_health"])
