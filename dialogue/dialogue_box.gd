extends Node

var dialogue
var dialogue_index = 0
var letter_index = 0

func _ready():
	$"NameLabel".text = dialogue[dialogue_index]["name"]
	$"TextLabel".text = ""

func _process(delta):
	if $"TextLabel".text != dialogue[dialogue_index]["text"]:
			$"TextLabel".text += dialogue[dialogue_index]["text"][letter_index]
			letter_index += 1

func _on_ProgressTimer_timeout():
	if dialogue_index < dialogue.size() - 1:
		dialogue_index += 1
		$"NameLabel".text = dialogue[dialogue_index]["name"]
		$"TextLabel".text = ""
		letter_index = 0
	else:
		queue_free()
		DialogueSystem.playing_dialogue = false
