extends KinematicBody

export var movement_speed = 1
export var vertical_speed = 1

var horizontal = 0
var vertical = 0
var jump_height = 0
var horizontal_speed = 0
var falling_speed = 0
var is_interacting = false
var is_jumping = false

func _ready():
	horizontal = 0
	jump_height = 3 * vertical_speed
	horizontal_speed = 20

func _process(delta):
	if Input.is_action_just_pressed("ui_left"):
		horizontal -= horizontal_speed
	if Input.is_action_just_pressed("ui_right"):
		horizontal += horizontal_speed

	if Input.is_action_just_released("ui_left"):
		horizontal += horizontal_speed
	if Input.is_action_just_released("ui_right"):
		horizontal -= horizontal_speed

	if Input.is_action_just_pressed("ui_up") and !is_jumping:
		jump()

	if is_on_ceiling():
		vertical = 0

#	if is_interacting: $"Sprite/AnimationPlayer".play("interacting")
#	elif horizontal != 0 and is_on_floor(): $"Sprite/AnimationPlayer".play("run")
#	elif horizontal == 0 and is_on_floor(): $"Sprite/AnimationPlayer".play("idle")
#	else: $"Sprite3D/AnimationPlayer".play("jump")

	if horizontal < 0: 
		$"Sprite3D".flip_h = false
	elif horizontal > 0: 
		$"Sprite3D".flip_h = true

	#"Gravity"
	if !is_on_floor():
		vertical -= delta * falling_speed * vertical_speed
		falling_speed += 0.1
	else:
		falling_speed = 0
		if vertical < 0: is_jumping = false

	if !is_interacting: move_and_slide(Vector3(horizontal, vertical, 0)*movement_speed, Vector3.UP)


func jump():
	is_jumping = true
	vertical = jump_height
	falling_speed = 0
