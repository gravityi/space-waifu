extends Node

const DialogueBox = preload("res://dialogue/DialogueBox.tscn")

var data
var playing_dialogue = false

func _ready():
	var file = File.new()
	file.open("res://data/dialogue.json", File.READ)
	data = parse_json(file.get_as_text())
	
#	# Test dialogue
#	yield(get_tree().create_timer(2), "timeout")
#	start_dialogue("tutorial01")
	
func start_dialogue(dialogue_key):
	if !playing_dialogue:
		var NewBox = DialogueBox.instance()
		NewBox.dialogue = data[dialogue_key]
		add_child(NewBox)
		playing_dialogue = true
