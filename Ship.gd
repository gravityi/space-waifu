extends Node2D

const Breach = preload("res://Breach.tscn")
const Toll = preload("res://Toll.tscn")
const Asteroid = preload("res://Asteroid.tscn")
const breach_locations = [Vector2(270, -60), Vector2(870, -60), Vector2(446, 240), Vector2(332, -374)]

var progress = 0
var current_breaches = []

func _ready():
	randomize()
	progress = randi() % 125 + 75
	$"BreachSpawnTimer".wait_time = rand_range(15, 25)
	$"TollSpawnTimer".wait_time = rand_range(30, 40)
	$"AsteroidSpawnTimer".wait_time = rand_range(60, 70)
#	$"BreachSpawnTimer".start()
#	$"TollSpawnTimer".start()
#	$"AsteroidSpawnTimer".start()

func get_breach_position():
	var new_pos = breach_locations[randi() % breach_locations.size()]
	if new_pos in current_breaches:
		return get_breach_position()
	else:
		return new_pos

func spawn_breach():
	if current_breaches.size() < 4:
		var NewObject = Breach.instance()
		var new_pos = get_breach_position()
		NewObject.position = new_pos
		current_breaches.append(new_pos)
		add_child(NewObject)
	$"BreachSpawnTimer".wait_time = rand_range(10, 20)

func spawn_toll():
	var NewObject = Toll.instance()
	NewObject.position = Vector2(1064, -84)
	add_child(NewObject)

func spawn_asteroid():
	var NewObject = Asteroid.instance()
	add_child(NewObject)
	$"AsteroidSpawnTimer".wait_time = rand_range(40, 50)

func _on_ProgressTimer_timeout():
	progress -= 1
	if progress <= 0:
		Transition.fade_to_scene("res://Shop.tscn")
