extends Control

var price_dict = {}

func _ready():
	randomize()
	price_dict["oxygen"] = randi() % 500 + 300
	price_dict["repair"] = randi() % 500 + 300
	price_dict["cargo"] = randi() % 899 + 100
	price_dict["fine"] = randi() % 1000 + 999
	update_display()
	
func update_display():
	$"VBoxContainer/HBoxContainer/Quantity".text = str(PlayerResources.resources_dict["cargo"]) + " / " + str(PlayerResources.resource_limits["cargo"])
	$"VBoxContainer/HBoxContainer/Quantity2".text = str(PlayerResources.resources_dict["oxygen"]) + " / " + str(PlayerResources.resource_limits["oxygen"])
	$"VBoxContainer/HBoxContainer/Quantity3".text = str(PlayerResources.resources_dict["fines"]) + " / " + str(PlayerResources.resource_limits["fines"])
	$"VBoxContainer/HBoxContainer2/Quantity".text = str(PlayerResources.resources_dict["ship_health"]) + " / " + str(PlayerResources.resource_limits["ship_health"])
	$"VBoxContainer/HBoxContainer2/Quantity2".text = str(PlayerResources.resources_dict["money"])
	$"VBoxContainer/Cargo/Price".text = "$" + str(price_dict["cargo"])
	$"VBoxContainer/Fine/Price".text = "$" + str(price_dict["fine"])
	$"VBoxContainer/Oxygen/Price".text = "$" + str(price_dict["oxygen"])
	$"VBoxContainer/Repair/Price".text = "$" + str(price_dict["repair"])


func _on_Oxygen_Buy_pressed():
	if PlayerResources.resources_dict["oxygen"] < PlayerResources.resource_limits["oxygen"] and PlayerResources.resources_dict["money"] >= price_dict["oxygen"]:
		PlayerResources.resources_dict["oxygen"] = clamp(PlayerResources.resources_dict["oxygen"] + 10, 0, PlayerResources.resource_limits["oxygen"])
		PlayerResources.resources_dict["money"] -= price_dict["oxygen"]
		update_display()


func _on_Oxygen_Sell_pressed():
	if PlayerResources.resources_dict["oxygen"] >= 10:
		PlayerResources.resources_dict["oxygen"] -= 10
		PlayerResources.resources_dict["money"] += price_dict["oxygen"]
		update_display()

func _on_Cargo_Buy_pressed():
	if PlayerResources.resources_dict["cargo"] < PlayerResources.resource_limits["cargo"] and PlayerResources.resources_dict["money"] >= price_dict["cargo"]:
		PlayerResources.resources_dict["cargo"] = clamp(PlayerResources.resources_dict["cargo"] + 50, 0, PlayerResources.resource_limits["cargo"])
		PlayerResources.resources_dict["money"] -= price_dict["cargo"]
		update_display()

func _on_Cargo_Sell_pressed():
	if PlayerResources.resources_dict["cargo"] >= 50:
		PlayerResources.resources_dict["cargo"] -= 50
		PlayerResources.resources_dict["money"] += price_dict["cargo"]
		update_display()

func _on_Repair_Buy_pressed():
	if PlayerResources.resources_dict["ship_health"] < PlayerResources.resource_limits["ship_health"] and PlayerResources.resources_dict["money"] >= price_dict["repair"]:
		PlayerResources.resources_dict["ship_health"] = PlayerResources.resource_limits["ship_health"]
		PlayerResources.resources_dict["money"] -= price_dict["repair"]
		update_display()

func _on_Fine_Buy_pressed():
	if PlayerResources.resources_dict["fines"] > 0 and PlayerResources.resources_dict["money"] >= price_dict["fine"]:
		PlayerResources.resources_dict["fines"] -= 1
		PlayerResources.resources_dict["money"] -= price_dict["fine"]
		update_display()

func _on_Travel_pressed():
	Transition.fade_to_scene("res://Ship.tscn")
