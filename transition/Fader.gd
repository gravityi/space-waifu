extends TextureRect

signal faded

var fade_out
var next_scene

func _ready():
	texture = ProxyTexture.new()
	expand = true
	stretch_mode = TextureRect.STRETCH_SCALE
	rect_size = get_viewport().size
	rect_global_position = get_viewport().global_canvas_transform.get_origin()
# warning-ignore:return_value_discarded
	connect("faded", Transition, "on_fade_out", [next_scene])

#	For testing purposes
#	yield(get_tree().create_timer(2), "timeout")
#	fade_out()

func _process(_delta):
	modulate[3] = lerp(modulate[3], int(fade_out), 0.1)
	if is_equal_approx(modulate[3], 0) and !fade_out: queue_free()
	elif is_equal_approx(modulate[3], 1) and fade_out and next_scene != null: emit_signal("faded")
