extends Area2D

onready var Player = $"../Player"
onready var ProgressTimer = $"../ProgressTimer"

func _ready():
	PlayerResources.toll_gate = true
	ProgressTimer.stop()
	$"Sprite/AnimationPlayer".play("shake")

func _process(delta):
	if Player in get_overlapping_bodies():
		if Input.is_action_just_pressed("interact") and !Player.is_interacting:
			$"TollTimer".stop()
			$"InteractionTimer".start()
			Player.is_interacting = true

func _on_InteractionTimer_timeout():
	Player.is_interacting = false
	ProgressTimer.start()
	$"../TollSpawnTimer".wait_time = rand_range(20, 30)
	$"../TollSpawnTimer".start()
	PlayerResources.toll_gate = false
	queue_free()

func _on_TollTimer_timeout():
	PlayerResources.resources_dict["fines"] += 1
	if PlayerResources.resources_dict["fines"] >= 3:
		Transition.fade_to_scene("res://GameOver.tscn")
