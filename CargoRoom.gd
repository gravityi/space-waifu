extends Area2D

func _on_CargoRoom_body_entered(body):
	$"ActionTimer".start()

func _on_CargoRoom_body_exited(body):
	$"ActionTimer".stop()

func _on_ActionTimer_timeout():
	$"../".progress -= 1
	if $"../".progress <= 0:
		Transition.fade_to_scene("res://Shop.tscn")
