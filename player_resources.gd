extends Node

var asteroid_incoming = false
var toll_gate = false
var shielded = false

var resources_dict = {
	"oxygen": 100,
	"ship_health": 0,
	"cargo" : 500,
	"money": 10000,
	"fines": 0
}

var resource_limits = {
	"oxygen": 100,
	"ship_health": 100,
	"cargo": 1000,
	"money": 999999,
	"fines": 3
}

func deal_damage():
	if shielded:
		PlayerResources.resources_dict["ship_health"] -= randi() % 15
	else:
		PlayerResources.resources_dict["ship_health"] -= randi() % 10 + 20
	asteroid_incoming = false
	if PlayerResources.resources_dict["ship_health"] <= 0:
		Transition.fade_to_scene("res://GameOver.tscn")
